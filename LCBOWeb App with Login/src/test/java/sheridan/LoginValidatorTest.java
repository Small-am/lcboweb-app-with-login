package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest 
{

	@Test
	public void testIsValidLoginRegular() 
	{
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("ramses"));
	}
	
	@Test
	public void testIsValidLoginRegularCount() 
	{
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("rams1234"));
	}
	
	@Test
	public void testIsValidLoginBoundaryIn() 
	{
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("ramses12"));
	} 
	
	@Test
	public void testIsValidLoginBoundaryInCount() 
	{
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("Kevin23"));
	} 
	
	@Test
	public void testIsValidLoginBoundaryOut() 
	{
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("Kev2"));
	}
	
	@Test
	public void testIsValidLoginBoundaryOutCount() 
	{
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("Kev"));
	}
	
	@Test
	public void testIsValidLoginException() 
	{
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("Kev3"));
	}
	
	@Test
	public void testIsValidLoginExceptionCount() 
	{
		assertFalse("Invalid login" , LoginValidator.isValidLoginName(""));
	}
}
