package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		if (loginName.length() < 6) 
			return false;

        
        for (int i = 0; i < loginName.length(); i++) {

        	char ch = loginName.charAt(i);

            if (is_Numeric(ch)) 
            	return true;
            else if (is_Letter(ch)) 
            	return true;
            else return false;
        }
        return true;
	}
	
	public static boolean is_Letter(char ch) {
        ch = Character.toUpperCase(ch);
        return (ch >= 'A' && ch <= 'Z');
    }
	
	public static boolean is_Numeric(char ch) {

        return (ch >= '0' && ch <= '9');
    }
}
